import psycopg2
import os


try:
    destinationDatabaseHost = "localhost"
    targetDatabaseUserName = "postgres"
    targetDatabasePassword = ""
    targetDatabaseName = "scrapper"

except Exception as e:
       print "excpetion while reading db config parameters" + str(e)

    
class dbConn(object):

  def __init__(self):
      conn_str = "dbname=%s user=%s host=%s password=%s" % (targetDatabaseName,
                                                          targetDatabaseUserName,
                                                          destinationDatabaseHost,
                                                          targetDatabasePassword)
      self.conn = psycopg2.connect(conn_str)
      self.curr =  self.conn.cursor()

  @staticmethod 
  def saveintoSrapperDb(insert_query,self):
      try:
          self.curr.execute(insert_query)
          self.conn.commit()
          return True
      except Exception as e:
          return False
          print "excpetion while saving into db " , insert_query , str(e)


  @staticmethod 
  def getNumbersFromSrapperDb(query,self):
      try:
          self.curr.execute(query)
          mobile_records = self.curr.fetchall() 
          return mobile_records
      except Exception as e:
          return []
          print "excpetion in getNumbersFromSrapperDb " , query , str(e)

  @staticmethod 
  def updateintoSrapperDb(update_query,self):
      try:
          self.curr.execute(update_query)
          self.conn.commit()
          return True
      except Exception as e:
          return False
          print "excpetion while updating into db " , insert_query , str(e)


  @staticmethod 
  def closeConn(self):
      self.conn.close()
      self.curr.close()
