import requests
from bs4 import BeautifulSoup
import csv
import time
from multiprocessing.dummy import Pool as ThreadPool
from datetime import datetime
from scrapLogger import scrapLogger
from dbConn import dbConn

#Used headers/agent because the request was timed out and asking for an agent. 
#Using following code we can fake the agent.
print "start time " + str(time.time())
scrapLogger = scrapLogger()
dbConn = dbConn()

def writeToDb(scrappedContent,flag):
    try: 
        insert_query = "INSERT INTO dnd_info ("
        insert_string = ""
        values_string = "("
        for k, v in scrappedContent.items():
            if k == 'Status':
               insert_string = insert_string + "status,"
               values_string = values_string + "'" + v + "'"','
            elif k == 'Service Provider':
                 insert_string = insert_string + "service_provider,"
                 values_string = values_string + "'" +  v + "'"','
            elif k == 'Your Preference is ':
                 insert_string = insert_string + "preference,"
                 values_string = values_string + "'" +  v + "'"','
            elif k == 'Phone Number':
                 insert_string = insert_string + "phone,"
                 values_string = values_string + "'" +  v + "'"','
            elif k == 'Service Area':
                 insert_string = insert_string + "area,"
                 values_string = values_string + "'" + v + "'"','
            elif k == 'Activation Date is':
                 insert_string = insert_string + "activation_date,"
                 values_string = values_string + "'" + v + "'"','
        insert_string = insert_string.rstrip(',')
        values_string = values_string.rstrip(',')
        insert_query = insert_query + insert_string + ") VALUES " + values_string + ")"
        print "insert_query " + str(insert_query)
        if dbConn.saveintoSrapperDb(insert_query,dbConn) == True:
           update_query = "Update numbers SET is_processed = 'true' where phone = '" + str(scrappedContent['Phone Number']) + "'"
           dbConn.updateintoSrapperDb(update_query,dbConn)
    except Exception as e:
           scrapLogger.writeErrorLog(scrapLogger,"error in writing to db " + str(e))
           print "error in writing to db" + str(e)
        
        
def writeToCSV(scrappedContent,flag):
    try:
        keys = (scrappedContent[0]).keys()
        print (keys)
        with open('scrap.csv', 'a') as output_file:
             dict_writer = csv.DictWriter(output_file, fieldnames=keys)
             if flag == True:
                dict_writer.writeheader()
             dict_writer.writerows(scrappedContent)
             scrapLogger.writeInfoLog(scrapLogger,"wrote the following row to the csv file " + str(scrappedContent))
             print (scrappedContent)
    except Exception as e:
           scrapLogger.writeErrorLog(scrapLogger,"error in writing to csv " + str(e))
           print "error in writing to csv" + str(e)

def task1(list_tr,flag):
    #list_rest =[]
    i=0
    dict = {}
    while i<len(list_tr):
        item = (list_tr[i]).get_text()
        if item == ":" :
           dict[list_tr[i-1].get_text()] = list_tr[i+1].get_text()
        i=i+1
    #list_rest.append(dict)
    writeToDb(dict,flag)

def scrapGovtSite(obj):
    headers = {'Content-Type': 'application/x-www-form-urlencoded',
    'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36',
    'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
    'Accept-Encoding':'gzip, deflate',
    'Accept-Language':'en-US,en;q=0.9',
    'Cache-Control':'max-age=0',
    'Connection':'keep-alive',
    'Content-Length':'18',
    'Cookie':'JSESSIONID=07A2725034154071690B0783DB0E75FD.node2; JSESSIONID=7830F7745E2FDFE6506ECBD990CB5EA3.node2',
    'Host':'www.nccptrai.gov.in',
    'Origin':'http://www.nccptrai.gov.in',
    'Referer': 'http://www.nccptrai.gov.in/nccpregistry/saveSearchSub.misc',
    'Upgrade-Insecure-Requests': '1'
    }

    try :
        flag = False
        PARAMS = {}
        number = obj['number']
        PARAMS['phoneno'] = number
        response = requests.post("http://www.nccptrai.gov.in/nccpregistry/saveSearchSub.misc",headers=headers,data=PARAMS)
        content = response.content
        soup = BeautifulSoup(content,"html.parser")
        rating_data_block = soup.find_all("table",attrs={"width":"800px"})
        list_tr=rating_data_block[0].find_all("td")
        if obj['count'] == 1:
           flag = True
        else:
           flag = False
        task1(list_tr,flag)
        #scrapLogger.writeInfoLog(scrapLogger,"response recieved from govt site for the number " + str(number) + " is " + str(list_tr))
        time.sleep(1)
    except Exception as e:
          scrapLogger.writeErrorLog(scrapLogger,"error while scrapping " + str(e))
          print ("error while scrapping" + str(e))


def processNumbers():
    try:
       """
       with open('numbers.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                   line_count += 1
                else:
                   yield row[0]
                   line_count += 1
       scrapLogger.writeInfoLog(scrapLogger,"total numbers readed from csv " + str(line_count - 1))
       """
       query = "Select phone from numbers where is_processed = 'false'"
       numbers = dbConn.getNumbersFromSrapperDb(query,dbConn)
       line_count = 0
       for row in numbers:
           yield row[0]
           line_count += 1
       scrapLogger.writeInfoLog(scrapLogger,"total numbers to process " + str(line_count))
    except Exception as e:
           scrapLogger.writeErrorLog(scrapLogger,"error in reading from numbers csv " + str(e))
           print ("error in reading from numbers csv" + str(e))

f = processNumbers()

t = ThreadPool(4)
count = 0
for i in f:
    count = count+1
    obj = {}
    obj['number'] = i
    obj['count'] = count
    t.map(scrapGovtSite, (i,obj))

t.close()
t.join()
print "end time " + str(time.time())

