
import datetime
import logging
import os
from logging.handlers import RotatingFileHandler
 

    
class scrapLogger(object):

  def __init__(self):
      self.logger = logging.getLogger(__name__)

  @staticmethod 
  def setInfoLog(self):
      try:
          info_handler = RotatingFileHandler(os.path.abspath(os.path.dirname(__file__)) +
                                    '/logs/scrapInfo.log', maxBytes=20000000,
                                  backupCount=1)
          info_handler.setLevel(logging.WARNING)
          info_format = logging.Formatter('%(asctime)s - %(name)s  - %(message)s')
          info_handler.setFormatter(info_format)
          self.logger.addHandler(info_handler)
      except Exception as e:
          print "excpetion while setting info log " ,  str(e)


  @staticmethod 
  def setErrorLog(self):
      try:
          error_handler = RotatingFileHandler(os.path.abspath(os.path.dirname(__file__)) +
                                    '/logs/scrapError=kh.log', maxBytes=20000000,
                                  backupCount=1)
          error_handler.setLevel(logging.ERROR)
          error_format = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
          error_handler.setFormatter(error_format)
          self.logger.addHandler(error_handler)
      except Exception as e:
          print "excpetion while setting error log " ,  str(e)

  
  @staticmethod 
  def writeInfoLog(self,data):
      try:
          self.logger.warning(data)
      except Exception as e:
          print "excpetion while writing info log " ,  str(e)



  @staticmethod 
  def writeErrorLog(self,data):
      try:
          self.logger.error(data)
      except Exception as e:
          print "excpetion while writing error log " , str(e)

self = scrapLogger()
scrapLogger.setInfoLog(self)
scrapLogger.setErrorLog(self)
